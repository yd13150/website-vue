import Qs from 'qs'
export default{

  url: '/get',

  // baseURL: 'http://192.168.1.195:8080', // 测试环境IP
  // baseURL: 'http://120.78.197.108:80', // 生产环境IP
  // baseURL: 'http://120.78.197.108:8090',
  baseURL: 'http://www.maizhubaovip.com',
  // baseURL: '',
  method: 'get',

  transformRequest: [
    function (data) {
      // 为了避免qs格式化时对内层对象的格式化先把内层的对象转为
      // data.strSQL = base64encode(data.strSQL)
      // 由于使用的form-data传数据所以要格式化
      data = Qs.stringify(data)
      return data
    }
  ],

  transformResponse: [
    function (data) {
      return data
    }
  ],

  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  },

  params: {},

  paramsSerializer: function (params) {
    return Qs.stringify(params)
  },

  data: {
    // appId: 1,
    // appName: '',
    // siteId: 1,
    // sessionId: (function () {
    //   if (!localStorage.hasOwnProperty('sessionId')) {
    //     localStorage.sessionId = new Date().getTime() + Math.random().toString().substring(2)
    //   }
    //   return localStorage.sessionId
    // })()
  },

  timeout: 10000,

  withCredentials: true, // default is false

  responseType: 'json', // default

  // 将upload事件注释掉，防止跨域状态下发起option请求

  // onUploadProgress: function(progressEvent) {
  // Do whatever you want with the native progress event
  // },

  // onDownloadProgress: function(progressEvent) {
  // Do whatever you want with the native progress event
  // },

  maxContentLength: 2000,

  validateStatus: function (status) {
    return status >= 200 && status < 300 // default
  },

  maxRedirects: 5 // default
}
