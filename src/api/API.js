import axios from 'axios'
import config from './config'

class API {
  url (url) {
    let base = 'mzb-upms-web'
    let reqURL = {
      // 用户留资接口
      saveCustomerInfo: base + '/merchant/retainedInfo/save',
      // 获取验证码接口
      getCaptcha: base + '/upms/user/getCaptcha',
      // 用户认证接口
      saveCertification: base + '/merchant/certification/save',
      // 获取七牛token信息
      getToken: base + '/qiniu/token/getToken'
    }
    return reqURL[url]
  }
  //  get请求
  get (path, params, fun, error) {
    path = path + '?'
    for (var item in params) {
      let str = item + '=' + params[item] + '&'
      path += str
    }
    path = path.substr(0, path.length - 1)
    axios.get(path, config).then(res => {
      if (!res.hasOwnProperty('data')) {
        console.log('no data in res')
        return
      }
      var data = res.data
      if (typeof (data) === 'string') {
        try {
          data = JSON.parse(data)
        } catch (ex) {
          data = {}
          return
        }
      }
      fun(data)
    }).catch(function () {
      error()
    })
  }

  // post请求
  post (path, param, fun, error) {
    config.params = param
    axios.post(path, {}, config).then(res => {
      if (!res.hasOwnProperty('data')) {
        console.log('no data in res')
        return
      }
      var data = res.data
      if (typeof (data) === 'string') {
        try {
          data = JSON.parse(data)
        } catch (ex) {
          data = {}
          return
        }
      }
      fun(data)
    }).catch(function () {
      error()
    })
  }
}
export default API
